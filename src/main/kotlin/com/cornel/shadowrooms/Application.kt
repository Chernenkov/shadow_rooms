package com.cornel.shadowrooms

import com.cornel.shadowrooms.core.common.Action
import com.cornel.shadowrooms.core.common.ActionType
import com.cornel.shadowrooms.core.common.StepType
import com.cornel.shadowrooms.core.engine.Engine
import com.cornel.shadowrooms.core.entity.player.Player
import com.cornel.shadowrooms.core.session.Session
import com.cornel.shadowrooms.front.Frontend
import com.cornel.shadowrooms.front.telegram.TelegramBot
import org.telegram.telegrambots.ApiContextInitializer

interface Application {
    fun registerUser(userName: String, gameId: Long)
    fun doAction(gameId: Long, action: Action)
    fun sendMessage(session: Session, text: String)
}

class ShadowRooms: Application {
    private val frontend: Frontend = TelegramBot(this)
    private val sessions = hashMapOf<Long, Session>()
    private val engine = Engine(this)

    init {
        frontend.start()
    }

    override fun registerUser(userName: String, gameId: Long) {
        val session = Session(Player(userName), id = gameId)
        if (sessions.containsKey(gameId)) {
            sessions.remove(gameId)
        }
        sessions.put(gameId, session)
        println("Registered $userName with gameId $gameId")
        doAction(gameId, Action("Следущая комната", ActionType.SEE_AROUND))
    }

    override fun doAction(gameId: Long, action: Action) {
        println("Action ${action.type} in game $gameId")
        val session = sessions[gameId]!!
        if (action.type == ActionType.START) {
            registerUser(session.player.name, gameId)
        } else {
            val nextStep = engine.nextStep(action, session)
            when (nextStep.type) {
                StepType.NEW_ACTIONS -> frontend.sendActions(gameId, nextStep.message!!, nextStep.actions!!)
                StepType.UPDATE_ACTIONS -> frontend.updateActions(gameId, nextStep.message!!, nextStep.actions!!)
            }
        }
    }

    override fun sendMessage(session: Session, text: String) = frontend.sendMessage(session.id, text)
}

fun main(args: Array<String>) {
    ApiContextInitializer.init()
    ShadowRooms()
}
