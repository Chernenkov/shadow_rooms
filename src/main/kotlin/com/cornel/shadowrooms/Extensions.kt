package com.cornel.shadowrooms

import java.util.*

fun Any?.isNull() = this == null


fun ClosedRange<Int>.random() =
        Random().nextInt(endInclusive - start) +  start