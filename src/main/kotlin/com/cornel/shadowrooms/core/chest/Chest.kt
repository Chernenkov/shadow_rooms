package com.cornel.shadowrooms.core.chest

import com.cornel.shadowrooms.core.common.Inventory
import com.cornel.shadowrooms.core.item.Item

class Chest(items: MutableList<Item>) : Inventory(items) {
    override fun add(item: Item) = false

    override fun remove(item: Item): Boolean {
        items.remove(item)
        return true
    }

    override fun replace(item: Item, with: Item) = false

    fun fillChest(){
        ChestGenerator.generate()
    }
}