package com.cornel.shadowrooms.core.chest

import com.cornel.shadowrooms.core.item.Item
import com.cornel.shadowrooms.core.item.armour.Armour
import com.cornel.shadowrooms.core.item.armour.bodyarmour.LeatherBodyArmour
import com.cornel.shadowrooms.core.item.armour.bodyarmour.MythrilBodyArmour
import com.cornel.shadowrooms.core.item.armour.bodyarmour.SteelBodyArmour
import com.cornel.shadowrooms.core.item.armour.bodyarmour.WroughtIronBodyArmor
import com.cornel.shadowrooms.core.item.armour.helmet.LeatherHelmet
import com.cornel.shadowrooms.core.item.armour.helmet.MythrilHelmet
import com.cornel.shadowrooms.core.item.armour.helmet.SteelHelmet
import com.cornel.shadowrooms.core.item.armour.helmet.WroughtIronHelmet
import com.cornel.shadowrooms.core.item.armour.pants.LeatherPants
import com.cornel.shadowrooms.core.item.armour.pants.MythrilPants
import com.cornel.shadowrooms.core.item.armour.pants.WroughtIronPants
import com.cornel.shadowrooms.core.item.armour.shield.LeatherShield
import com.cornel.shadowrooms.core.item.armour.shield.MythrilShield
import com.cornel.shadowrooms.core.item.armour.shield.SteelShield
import com.cornel.shadowrooms.core.item.armour.shield.WroughtIronShield
import com.cornel.shadowrooms.core.item.weapon.Weapon
import com.cornel.shadowrooms.core.item.weapon.knife.BronzeKnife
import com.cornel.shadowrooms.core.item.weapon.knife.MythrilKnife
import com.cornel.shadowrooms.core.item.weapon.knife.SteelKnife
import com.cornel.shadowrooms.core.item.weapon.knife.WroughtIronKnife
import com.cornel.shadowrooms.core.item.weapon.sword.*
import com.cornel.shadowrooms.random
import java.util.*

class ChestGenerator(){

    companion object {
        fun generate(): Chest {
            val items = mutableListOf<Item>()
            for (i in 1..(1..3).random()) {
                items.add(getItem(Math.random()))
            }
            return Chest(items)
        }

        private fun getItem(rand: Double) = when (rand) {
            in (0.0..0.5) -> generateWeapon(rand)
            else -> generateArmour(rand)
        }


        private fun generateWeapon(rand: Double) : Weapon = when (rand) {
            in (0.0..0.07) -> BronzeKnife()
            in (0.8..0.13) -> BronzeTwoHandedSword()
            in (0.14..0.21) -> BronzeKnife()
            in (0.22..0.26) -> WroughtIronKnife()
            in (0.27..0.31) -> WroughtIronSword()
            in (0.32..0.34) -> WroughtIronTwoHandedSword()
            in (0.35..0.37) -> MythrilKnife()
            in (0.38..0.41) -> SteelKnife()
            in (0.42..0.45) -> SteelSword()
            in (0.451..0.46) -> SteelTwoHandedSword()
            in (0.461..0.49) -> MythrilSword()
            in (0.491..0.5) -> MythrilTwoHandedSword()
            else -> BronzeKnife()
        }

        private fun generateArmour(rand: Double) : Armour = when (rand) {
            in (0.51..0.55) -> LeatherBodyArmour()
            in (0.56..0.58) -> MythrilBodyArmour()
            in (0.59..0.6) ->  SteelBodyArmour()
            in (0.61..0.67) -> WroughtIronBodyArmor()
            in (0.68..0.7) -> LeatherHelmet()
            in (0.71..0.72) -> MythrilHelmet()
            in (0.721..0.73) -> SteelHelmet()
            in (0.74..0.75) -> WroughtIronHelmet()
            in (0.751..0.8) -> LeatherPants()
            in (0.81..0.82) -> MythrilPants()
            in (0.821..0.83) -> WroughtIronPants()
            in (0.831..0.834) -> MythrilShield()
            in (0.835..0.89) -> LeatherShield()
            in (0.9..0.95) -> SteelShield()
            in (0.95..1.0) -> WroughtIronShield()
            else -> LeatherPants()

        }
    }


}