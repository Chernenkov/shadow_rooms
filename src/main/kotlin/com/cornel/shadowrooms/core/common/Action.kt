package com.cornel.shadowrooms.core.common

data class Action(val name: String, val type: ActionType, val payload: String = "empty")