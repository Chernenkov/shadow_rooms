package com.cornel.shadowrooms.core.common

enum class ActionType {
    SEE_AROUND,
    NEXT_ROOM,
    CHECK_STATS,

    CHECK_INVENTORY,
    INV_CHECK_ITEM,
    INV_EQUIP_ITEM,
    INV_THROW_ITEM,

    EQUIP_CHECK_ITEM,
    EQUIP_REMOVE_ITEM,
    EQUIP_THROW_ITEM,

    LOOT_CHEST,
    CHEST_TAKE_ITEM,

    CHECK_TRADER,
    BEGIN_BATTLE,

    HIT,

    HIDE,

    LEVEL_UP,

    START,

    UP_STAT
}