package com.cornel.shadowrooms.core.common

import com.cornel.shadowrooms.core.item.Item


/**
 * Base prototype for any item storage.
 *
 * Contains methods for adding and removing items.
 *
 * @property size space occupied by items in inventory
 * @property count a count of items in inventory
 * @property items a set of stored items
 * */
abstract class Inventory(val items: MutableList<Item>) {
    val size = items.fold(0) { acc, item -> acc + item.invSize }
    val count = items.size

    /**
     * Adds the [item] in the inventory
     *
     * @return is operation completed successfully
     * */
    abstract fun add(item: Item): Boolean

    /**
     * Removes the [item] from the inventory
     *
     * @return is operation completed successfully
     * */
    abstract fun remove(item: Item): Boolean

    /**
     * Removes [item] and adds [with].
     * */
    abstract fun replace(item: Item, with: Item): Boolean

    override fun toString() = StringBuilder().apply {
        items.forEachIndexed { index, it ->
            append("${index + 1}) $it\n")
        }
    }.toString()
}

