package com.cornel.shadowrooms.core.common

data class Step(val type: StepType, val message: String?, val actions: List<Action>?)

enum class StepType { NEW_ACTIONS, UPDATE_ACTIONS }