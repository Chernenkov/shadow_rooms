package com.cornel.shadowrooms.core.engine

import com.cornel.shadowrooms.core.common.Action
import com.cornel.shadowrooms.core.common.ActionType
import com.cornel.shadowrooms.core.entity.player.Stats
import com.cornel.shadowrooms.core.room.Room
import com.cornel.shadowrooms.core.session.Session

class ActionEngine {
    companion object {
        fun seeAround(session: Session): List<Action> {
            fun seeRoomSecure(session: Session): List<Action> {
                val nextActions = mutableListOf(
                        Action("Открыть дверь", ActionType.NEXT_ROOM),
                        Action("Открыть рюкзак", ActionType.CHECK_INVENTORY),
                        Action("Проверить самочувствие", ActionType.CHECK_STATS)
                )

                if (session.room.trader != null) {
                    nextActions.add(0, Action("Поговорить с торговцем", ActionType.CHECK_TRADER))
                }

                if (session.room.chest != null) {
                    nextActions.add(0, Action("Проверить сундук", ActionType.LOOT_CHEST))
                }

                if (session.player.exp >= session.player.getLevelUpExpAmount()) {
                    // Enough exp to level up
                    nextActions.add(0, Action("Поднять уровень", ActionType.LEVEL_UP))
                }
                return nextActions
            }
            fun seeRoomWarning(session: Session): List<Action> {
                val nextActions = mutableListOf(
                        Action("Вступить в бой", ActionType.BEGIN_BATTLE),
                        Action("Попытаться спрятаться", ActionType.HIDE),
                        Action("Открыть рюкзак", ActionType.CHECK_INVENTORY)
                )

                if (session.player.exp >= session.player.getLevelUpExpAmount()) {
                    // Enough exp to level up
                    nextActions.add(0, Action("Поднять уровень", ActionType.LEVEL_UP))
                }

                return nextActions
            }

            fun seeRoomDanger(session: Session) = listOf(
                Action("Открыть дверь", ActionType.NEXT_ROOM),
                Action("Вступить в бой", ActionType.BEGIN_BATTLE),
                Action("Проверить самочувствие", ActionType.CHECK_STATS)
            )

            return when (session.room.state) {
                Room.STATE_SECURE -> seeRoomSecure(session)
                Room.STATE_WARNING -> seeRoomWarning(session)
                Room.STATE_DANGER -> seeRoomDanger(session)
                else -> throw RuntimeException("Unknown room state: ${session.room.state}\n$this")
            }
        }

        fun checkInventory(session: Session): List<Action>? {
            val actions = mutableListOf(Action("Закрыть рюкзак", ActionType.SEE_AROUND))
            session.player.equip.equip.forEach {
                actions.add(Action("[${it.value}]", ActionType.EQUIP_CHECK_ITEM, it.key.toString()))
            }
            println(actions)
            session.player.inventory.items.forEachIndexed { index, item ->
                actions.add(Action("${index + 1}) ${item.name}", ActionType.INV_CHECK_ITEM, index.toString()))
            }

            return actions
        }

        fun nextBattleTurn(session: Session): List<Action>? {
            return listOf(Action("Ударить", ActionType.HIT))
        }

        fun lootChest(session: Session): List<Action>? {
            val actions = mutableListOf(Action("Закрыть", ActionType.SEE_AROUND))
            session.room.chest!!.items.forEachIndexed { index, item ->
                actions.add(Action("${index + 1}) ${item.name}", ActionType.CHEST_TAKE_ITEM, index.toString()))
            }
            return actions
        }

        fun invCheckItem(action: Action, session: Session) = listOf(
                Action("Экипировать", ActionType.INV_EQUIP_ITEM, action.payload),
                Action("Выбросить", ActionType.INV_THROW_ITEM, action.payload),
                Action("Положить обратно", ActionType.CHECK_INVENTORY)
        )

        fun equipCheckItem(action: Action, session: Session) = listOf(
                Action("Снять", ActionType.EQUIP_REMOVE_ITEM, action.payload),
                Action("Выбросить", ActionType.EQUIP_THROW_ITEM, action.payload),
                Action("Назад", ActionType.CHECK_INVENTORY)
        )

        fun levelUp(session: Session) = listOf(
            Action("Сила", ActionType.UP_STAT, Stats.STR.toString()),
            Action("Ловкость", ActionType.UP_STAT, Stats.DEX.toString()),
            Action("Вынослвость",  ActionType.UP_STAT, Stats.END.toString()),
            Action("Удача", ActionType.UP_STAT, Stats.FRT.toString())
        )
    }
}