package com.cornel.shadowrooms.core.engine

import com.cornel.shadowrooms.Application
import com.cornel.shadowrooms.core.common.Action
import com.cornel.shadowrooms.core.common.ActionType
import com.cornel.shadowrooms.core.common.Step
import com.cornel.shadowrooms.core.common.StepType
import com.cornel.shadowrooms.core.room.Room
import com.cornel.shadowrooms.core.session.Session

class Engine(private val application: Application) {
    fun nextStep(action: Action, session: Session): Step {
        // Получить экшены и сообщения исходя из последнего экшена и состояния в сессии.
        // Составить соотв. step и вернуть его

        return when (action.type) {
            ActionType.SEE_AROUND -> seeAround(action, session)
            ActionType.NEXT_ROOM -> nextRoom(action, session)
            ActionType.CHECK_STATS -> checkStats(action, session)
            ActionType.CHECK_INVENTORY -> checkInventory(action, session)
            ActionType.HIDE -> hide(action, session)
            ActionType.BEGIN_BATTLE -> beginBattle(action, session)
            ActionType.HIT -> nextBattleTurn(action, session)
            ActionType.LOOT_CHEST -> lootChest(action, session)
            ActionType.INV_CHECK_ITEM -> invCheckItem(action, session)
            ActionType.INV_EQUIP_ITEM -> invEquipItem(action, session)
            ActionType.INV_THROW_ITEM -> invThrowItem(action, session)
            ActionType.EQUIP_CHECK_ITEM -> equipCheckItem(action, session)
            ActionType.EQUIP_REMOVE_ITEM -> equipRemoveItem(action, session)
            ActionType.EQUIP_THROW_ITEM -> equipThrowItem(action, session)
            ActionType.CHEST_TAKE_ITEM -> chestTakeItem(action, session)
            ActionType.LEVEL_UP -> levelUp(action, session)
            ActionType.UP_STAT -> upStat(action, session)
            else -> throw RuntimeException("Unknown action type: ${action.type}")
        }
    }

    private fun upStat(action: Action, session: Session): Step {
        session.player.stats.add(action.payload.toInt())
        return seeAround(action, session)
    }

    private fun chestTakeItem(action: Action, session: Session): Step {
        val chest = session.room.chest!!
        val inventory = session.player.inventory
        val item = chest.items[action.payload.toInt()]
        if (item.invSize > inventory.space) {
            application.sendMessage(session, "Недостаточно места в инвентаре")
        } else {
            chest.remove(item)
            inventory.add(item)
        }
        return lootChest(action, session)
    }

    private fun equipThrowItem(action: Action, session: Session): Step {
        session.player.equip.takeOff(action.payload.toInt())
        return checkInventory(action, session)
    }

    private fun equipRemoveItem(action: Action, session: Session): Step {
        val player = session.player
        val part = action.payload.toInt()
        val item = player.equip[part]!!
        if (player.inventory.space < item.invSize) {
            application.sendMessage(session, "Недостаточно место в рюкзаке.")
        } else {
            player.equip.takeOff(item)
            player.inventory.add(item)
        }
        return checkInventory(action, session)
    }

    private fun equipCheckItem(action: Action, session: Session) = Step(StepType.UPDATE_ACTIONS,
            MessageEngine.equipCheckItem(action, session),
            ActionEngine.equipCheckItem(action, session))

    private fun invThrowItem(action: Action, session: Session): Step {
        val inventory = session.player.inventory
        inventory.remove(inventory.items[action.payload.toInt()])
        return checkInventory(action, session)
    }

    private fun invEquipItem(action: Action, session: Session): Step {
        val player = session.player
        val item = player.inventory.items[action.payload.toInt()]
        player.inventory.remove(item)
        val removed = player.equip.equip(item)
        val removedSize = removed.fold(0) { acc, it -> acc + it.invSize }
        if (player.inventory.space < removedSize) {
            application.sendMessage(session, "Недостаточно места в рюкзаке чтобы положить снятые вещи.")
            player.inventory.add(item)
            removed.forEach { player.equip.equip(it) }
        } else {
            removed.forEach { player.inventory.add(it) }
        }
        return checkInventory(action, session)
    }

    private fun invCheckItem(action: Action, session: Session) = Step(StepType.UPDATE_ACTIONS,
        MessageEngine.invCheckItem(action, session),
        ActionEngine.invCheckItem(action, session))

    private fun lootChest(action: Action, session: Session): Step {
        return Step(StepType.UPDATE_ACTIONS,
                MessageEngine.lootChest(session),
                ActionEngine.lootChest(session))
    }

    private fun nextRoom(action: Action, session: Session): Step {
        session.nextRoom()
        application.sendMessage(session, "На двери светится число: ${session.room.hashCode()}\n\n" +
                "Ты осторожно приоткрываешь её и заходишь в следущую комнату...")
        return seeAround(action, session)
    }

    private fun seeAround(action: Action, session: Session): Step {
        return Step(StepType.NEW_ACTIONS,
                MessageEngine.seeAround(session),
                ActionEngine.seeAround(session))
    }

    private fun checkStats(action: Action, session: Session): Step {
        application.sendMessage(session, session.player.getStatusString())
        return seeAround(action, session)
    }

    private fun checkInventory(action: Action, session: Session): Step {
        return Step(StepType.UPDATE_ACTIONS,
                MessageEngine.checkInventory(session),
                ActionEngine.checkInventory(session))
    }

    private fun hide(action: Action, session: Session): Step {
        val player = session.player
        val room = session.room
        val hide = player.dice() - 0.1 // 90% chance to hide with frt = 0

        if (hide > 0) {
            // Successfully hidden
            room.state = Room.STATE_DANGER
            application.sendMessage(session, "Ты скрываешься в тени...")
            return seeAround(action, session)
        } else {
            // Detected
            application.sendMessage(session, "Ты слишком громко топал, и ${room.monster!!.name} заметил(а) тебя!")
            return beginBattle(action, session)
        }
    }

    private fun beginBattle(action: Action, session: Session): Step {
        session.room.state = Room.STATE_BATTLE
        val monster = session.room.monster
        if (action.type == ActionType.HIDE) {
            // Unsuccessful hide
            if (monster == null) {
                throw RuntimeException("Monster is null")
            } else {
                val success = monster.hit(session.player)
                application.sendMessage(session, "${monster.name} бъёт первым и " +
                        if (success) "попадает" else "промахивается")
            }
        } else {
            application.sendMessage(session, "Ты влетаешь в монстра из тени")
        }
        return nextBattleTurn(action, session, true)
    }

    private fun nextBattleTurn(action: Action, session: Session, firstTurn: Boolean = false): Step {
        val player = session.player
        val monster = session.room.monster
        if (monster == null) {
            throw RuntimeException("Monster is null")
        }
        if (player.isAlive()) {
            val playerHit = player.hit(monster)
            if (monster.isAlive()) {
                val stepType = if (firstTurn) StepType.NEW_ACTIONS else StepType.UPDATE_ACTIONS
                val monsterHit = monster.hit(player)
                return Step(stepType,
                        MessageEngine.nextBattleTurn(session, playerHit, monsterHit),
                        ActionEngine.nextBattleTurn(session))
            } else {
                application.sendMessage(session, "${monster.name} побеждён")
                session.room.state = Room.STATE_SECURE
                session.player.exp += monster.expForKill
                session.room.monster = null
                return seeAround(action, session)
            }

        } else {
            return Step(StepType.NEW_ACTIONS, "Ты умер. Никогда такого не было, и вот опять!",
                    listOf(Action("Начать заново!", ActionType.START)))
        }
    }

    private fun levelUp(action: Action, session: Session) = Step(StepType.UPDATE_ACTIONS,
            "Выберите характеристику: ",
            ActionEngine.levelUp(session))
}
