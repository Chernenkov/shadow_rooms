package com.cornel.shadowrooms.core.engine

import com.cornel.shadowrooms.core.common.Action
import com.cornel.shadowrooms.core.room.Room
import com.cornel.shadowrooms.core.session.Session


class MessageEngine {
    companion object {
        fun seeAround(session: Session): String {
            val room = session.room
            val monster = room.monster
            val chest = room.chest
            val trader = room.trader
            return when (room.state) {
                Room.STATE_WARNING -> "Осторожно войдя в комнату, ты замечаешь существо в тени. Это ${monster!!.name}.\n" +
                        "Можно попробовать прокрасться мимо, либо вступить в бой."
                Room.STATE_BATTLE -> "Противник - ${monster!!.name}.\n\n${monster.name} ${monster.getHpString()}."
                Room.STATE_DANGER -> "Ты спрятался в тени и стараешься не дышать. Где-то рядом шарится ${monster!!.name}."
                Room.STATE_SECURE -> {
                    val message = StringBuilder()
                    message.append("Ты стоишь в комнате. Кажется, здесь пока безопасно.\n\n")

                    if (chest != null)
                        message.append("Ты замечаешь внушительных размеров сундук, стоящий в тёмном углу комнаты.\n\n")

                    if (trader != null)
                        message.append("Кто-то подзывает тебя подойти. Это торговец.\n\n")

                    message.append("В другом конце комнаты ты видишь дверь.\n\n")

                    message.toString()
                }
                else -> throw RuntimeException("Unknown room state ${room.state}.")
            }
        }

        fun checkInventory(session: Session) = StringBuilder().apply {
            append("На Вас надето:\n\n")
            append(session.player.equip.getAllItems())
            append("\nВы открываете ваш рюкзак.\n\nРюкзак ${session.player.inventory.getSpaceString()}.\n\n")
            append(session.player.inventory)
        }.toString()

        fun nextBattleTurn(session: Session, playerHit: Boolean, monsterHit: Boolean): String? {
            val monster = session.room.monster!!
            return StringBuilder().apply {
                append( monster.name + " говорит:\n" + monster.getPhrase())
                append("\n\n")
                append("Ты пытаешься ударить ${monster.name} и " +
                        if (playerHit) "попадаешь" else "промахиваешься")
                append("\n\n${monster.name} наносит ответный удар и " +
                        if (monsterHit) "попадает" else "промахивается")
                append("\n\n${monster.name} ${monster.getHpString()}\n")
                append("Ты ${session.player.getHpString()}")
            }.toString()
        }

        fun lootChest(session: Session) = StringBuilder().apply {
            append("Вы открываете сундук:\n\n${session.room.chest}")
        }.toString()

        fun invCheckItem(action: Action, session: Session): String? {
            val index = action.payload.toInt()
            val item = session.player.inventory.items[index]
            return "${item.name}\nСтоимость: ${item.cost} золотых"
        }

        fun equipCheckItem(action: Action, session: Session): String? {
            val part = action.payload.toInt()
            val item = session.player.equip[part]!!
            return "${item.name}\nСтоимость: ${item.cost} золотых"
        }
    }
}