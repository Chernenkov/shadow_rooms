package com.cornel.shadowrooms.core.entity

/**
 * Describes base properties of any entity.
 *
 * @property baseHp start hp level independent value of health points
 * @property hp     current hp level
 * */
abstract class Entity(
        val name: String,
        var level: Int,
        var hp: Int,                        //changed to default from protected to be able to modify in Monster
        protected val baseHp: Int) {
    /**
     * Injury current entity with [hp] value.
     * */
    abstract fun injury(hp: Int)

    /**
     * Heal current entity with [hp] value.
     *
     * If [hp] is not specified, heals fully
     * */
    abstract fun heal(hp: Int = getMaxHp())

    /**
     * Calculate random dodge value
     * */
    abstract fun dodge(): Double

    /**
     * Try to hit [what].
     *
     * @return true if the target is successfully hit
     * */
    abstract fun hit(what: Entity): Boolean

    /**
     * Check if the entity is alive.
     * */
    open fun isAlive(): Boolean = hp > 0
    /**
     * Calculate max amount of hp depends on level and stats.
     * */
    abstract fun getMaxHp(): Int


    /**
     * Generate random value from 0 to 1.
     *
     * Used in all random-based actions.
     * */
    abstract fun dice(): Double

    /**
     * Lifecycle method to make actions before each turn
     * */
    abstract fun beforeTurn()


    /**
     * Lifecycle method to make actions after each turn
     * */
    abstract fun afterTurn()

    open fun getHpString(): String = when {
        hp < 0.2 * getMaxHp()   -> "при смерти"
        hp < 0.4 * getMaxHp()   -> "очень серьёзно ранен(а)"
        hp < 0.6 * getMaxHp()   -> "серьёзно ранен(а)"
        hp < 0.8 * getMaxHp()   -> "ранен(а)"
        hp < getMaxHp()         -> "почти полностью здоров(а)"
        else                    -> "полностью здоров(а)"
    }
}