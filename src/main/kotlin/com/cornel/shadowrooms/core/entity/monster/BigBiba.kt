package com.cornel.shadowrooms.core.entity.monster

import com.cornel.shadowrooms.core.entity.monster.Monster
import java.io.File



class BigBiba : Monster("Биг Биба", 10, 500, 500, 20, 0.05, 300) {

    override val data = arrayOf(
            "Биба и боба - очень хорошие друзья",
            "Биба очень злиться",
            "Биба чувствовать боль",
            "*Биба болтается перед вашим носом*")

    override fun beforeTurn() {
    }

    override fun afterTurn() {
    }

}