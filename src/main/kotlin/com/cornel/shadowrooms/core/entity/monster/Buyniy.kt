package com.cornel.shadowrooms.core.entity.monster

import com.cornel.shadowrooms.core.entity.monster.Monster
import java.io.File



class Buyniy : Monster("Буйный", 9, 250, 250, 17, 0.1, 200) {

    override val data = arrayOf(
            "Мент, чего тебе надо, свали отсюда быстро!",
            "Чё те надо у меня дома?",
            "А зачем тебе мне видеть?!",
            "ДВЕРЬ!СДЕЛАЛ!",
            "ПОСТАВИЛ МНЕ ДВЕРЬ БЫСТРА!",
            "Дверь запили!",
            "Ставь прям как выпилил")

    override fun beforeTurn() {
    }

    override fun afterTurn() {
    }

}