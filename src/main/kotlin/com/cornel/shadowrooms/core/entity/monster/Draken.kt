package com.cornel.shadowrooms.core.entity.monster

class Draken : Monster("Дракен", 14, 700, 700, 60, 0.0, 500) {

    override val data = arrayOf(
            "Роарррр!!1",
            "Ты не убил дракена!",
            "Врум-врум-врум!!!111!",
            "Дракен в студии!11",
            "Рррррррр!!",
            "В дискуссионном клубе я бы разбил её в прах и пух!",
            "Уверяю вас, единственный способ избавиться от дракенов — это иметь своего собственного.")

    override fun beforeTurn() {
    }

    override fun afterTurn() {
    }

}