package com.cornel.shadowrooms.core.entity.monster

import com.cornel.shadowrooms.core.entity.monster.Monster
import java.io.File



class Khtulhu : Monster("Ктулху", 999, 1000, 1000, 100, 0.3, 1000 ) {

    override val data = arrayOf(
            "Беспочвенный оптимизм.",
            "Многовековое могильное безмолвие...",
            "Не вызывай того, кого не сможешь повергнуть.",
            "Не мёртво то, что в вечности пребудет, Со смертью времени и смерть умрёт.",
            "Я вешу в необычном душевном состоянии, потому что вечером меня не станет.",
            "Впечатлительность — удел ничтожеств.",
            "Мы живём на тихом островке невежества посреди темного моря бесконечности, и нам вовсе не следует плавать на далекие расстояния.")

    override fun beforeTurn() {
    }

    override fun afterTurn() {
    }

}