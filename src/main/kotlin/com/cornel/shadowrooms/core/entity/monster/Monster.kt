package com.cornel.shadowrooms.core.entity.monster

import com.cornel.shadowrooms.core.entity.Entity
import java.util.*

/**
 *
 * Describes basic properties for each monster
 * Monster has 60% probability to hit, and 30% probability to dodge from injury
 *
 * @param expForKill     number of experience points for killing current monster.
 *                       Living: 'small' gives 10 experience,
 *                       'middle' gives 15 experience, 'big' gives 20 experience
 *                       Undead: 'small' gives 15 experience, 'middle' gives 20 experience
 *                       'big' gives 25 experience
 *
 * @author Alex Chernenkov
 * */
abstract class Monster(
        name: String,
        level: Int,
        hp: Int,
        bHp: Int,
        val damage: Int,
        val dodge: Double,
        val expForKill: Int
) : Entity(name, level, hp, bHp) {

    abstract protected val data: Array<String>

    companion object {

        fun generate(): Monster = when(Math.random()){
            in (0.0..0.09) -> BigBiba()
            in (0.1..0.15) -> Buyniy()
            in (0.16..0.24) -> Draken()
            in (0.25..0.27) -> E6aka()
            in (0.28..0.32) -> Gazyuga()
            in (0.33..0.4) -> Muchozhuk()
            in (0.41..0.47) -> Petux()
            in (0.48..0.54) -> Piston()
            in (0.65..0.7) -> Rat()
            in (0.71..0.85) -> Skelet()
            in (0.86..0.97) -> Volk()
            in (0.98..0.99) -> Khtulhu()
            else -> Skelet()
        }

    }

    fun getPhrase() : String = data[Random().nextInt((data.size-1))]
    /**
     * Implements simple injury, decrementing hp for parameter [hp]
     */
    override fun injury(hp: Int) {
        this.hp -= hp
    }

    /**
     * @return true if hp of attacked entity was damaged
     */
    override fun hit(what: Entity): Boolean {
        val entityDodgeChance = what.dodge()

        val hit = Math.random() - entityDodgeChance

        return if (hit > 0) {
            what.injury(damage)
            true
        } else {
            false
        }
    }

    /**
     * @return true if the entity is alive
     */
    override fun isAlive(): Boolean = (hp > 0)

    /**
     * @param hp points on how many hp must be recovered, if hp
     *           overflows baseHp barrier, just sets entity's hp to baseHp
     */
    override fun heal(hp: Int) {
        this.hp = if((this.hp + hp) > baseHp) baseHp else (this.hp + hp)
    }

    /**
     * Generates random value from [0, 1] to set probability to hit
     */
    override fun dice(): Double = Math.random()

    /**
     * @return baseHp for monster, because it doesn't have any stats, so max hp is basic hp
     */
    override fun getMaxHp(): Int = this.baseHp

    /**
     * Generates random value from [0, 1] to set probability to dodge from injury
     */
    override fun dodge(): Double = dodge

}