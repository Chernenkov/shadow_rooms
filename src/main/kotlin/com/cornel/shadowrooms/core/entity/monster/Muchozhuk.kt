package com.cornel.shadowrooms.core.entity.monster

class Muchozhuk : Monster("Мухожук", 4, 70, 70, 5, 0.5, 90) {

    override val data = arrayOf(
            "Дай мне сахар. В воде. Ещё...",
            "Пожалуйста, положи винтовку на землю.",
            "Война, хорошо! Больше пищи для моих детей! 78 миллионов, их непросто прокормить.")

    override fun beforeTurn() {
    }

    override fun afterTurn() {
    }

}