package com.cornel.shadowrooms.core.entity.monster

class Piston : Monster("Пистон", 6, 200, 200, 15, 0.2, 150) {

    override val data = arrayOf(
            "Ничего вы тут не сделаете. Чёрная масть неистребима.",
            "Братва! У нас крыса в бараке!",
            "Будь паинькой, давай по-хорошему, а то больнее будет!",
            "Такого тебе кадра нарисовали! Образованный, революционер!")

    override fun beforeTurn() {
    }

    override fun afterTurn() {
    }

}