package com.cornel.shadowrooms.core.entity.monster

import com.cornel.shadowrooms.core.entity.monster.Monster
import java.io.File



class Rat : Monster("Крыса", 1, 35, 35, 4, 0.3, 30) {

    override val data = arrayOf(
            "*Писк*",
            "*Писк в стиле пост-рок*",
            "*Грустный писк*",
            "*Огорченный писк*",
            "*Обреченный писк*",
            "*Злобный писк*")

    override fun beforeTurn() {
    }

    override fun afterTurn() {
    }

}