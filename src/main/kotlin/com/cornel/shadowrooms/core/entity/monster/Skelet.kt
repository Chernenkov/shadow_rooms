package com.cornel.shadowrooms.core.entity.monster

import com.cornel.shadowrooms.core.entity.monster.Monster
import java.io.File



class Skelet : Monster("Скелет", 5, 150, 150, 10, 0.1, 90) {

    override val data = arrayOf(
            "*Шум костей*",
            "*Ребро отвалилось*",
            "*Отвалилась рука*",
            "*Протяжный вой*",
            "*Сломалась правая нога*")

    override fun beforeTurn() {
    }

    override fun afterTurn() {
    }

}