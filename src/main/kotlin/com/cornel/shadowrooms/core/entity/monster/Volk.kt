package com.cornel.shadowrooms.core.entity.monster

import com.cornel.shadowrooms.core.entity.monster.Monster
import java.io.File



class Volk : Monster("Волк", 3, 90, 90, 7, 0.2, 60) {

    override val data = arrayOf(
            "Аууууу!",
            "РРРрррр",
            "Вуф!",
            "*Кусает себя за хвост*",
            "*Грызет свою лапу*",
            "*Чешет за ухом*")

    override fun beforeTurn() {
    }

    override fun afterTurn() {
    }



}