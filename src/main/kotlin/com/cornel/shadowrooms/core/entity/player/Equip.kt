package com.cornel.shadowrooms.core.entity.player

import com.cornel.shadowrooms.core.item.Item
import com.cornel.shadowrooms.core.item.armour.Armour
import com.cornel.shadowrooms.core.item.armour.bodyarmour.BodyArmour
import com.cornel.shadowrooms.core.item.armour.helmet.Helmet
import com.cornel.shadowrooms.core.item.armour.pants.Pants
import com.cornel.shadowrooms.core.item.armour.shield.Shield
import com.cornel.shadowrooms.core.item.weapon.Weapon

/**
 * Player equipment
 *
 * @property equip a map of equipment
 * */
class Equip(val equip: MutableMap<Int, Item?> = mutableMapOf()) {

    /**
     * Returns [Item] equipped on provided [part] of body or null if nothing is equipped.
     * */
    operator fun get(part: Int) = equip[part]

    val head: Item?
        get() = get(HEAD)
    val body: Item?
        get() = get(BODY)
    val legs: Item?
        get() = get(LEGS)
    val mainHand: Item?
        get() = get(MHAND)
    val secondHand: Item?
        get() = get(SHAND)

    fun getDamage(): Int {
        val itemInHand = get(MHAND)
        return if (itemInHand != null && itemInHand is Weapon) {
            itemInHand.damage
        } else {
            0
        }
    }

    fun getAllItems(): String {
        val sb = StringBuilder()
        for (i in equip){
            sb.append("${i.value} \n")
        }
        return sb.toString()
    }

    fun getProtection(): Double {
        var protection = 0.0
        equip.forEach {
            if (it is Armour) {
                protection += it.defensePoints
            }
        }
        return protection
    }

    /**
     * Removes an item from selected body [part].
     *
     * @return removed item
     * */
    fun takeOff(part: Int): List<Item> {
        val removed = equip.remove(part)
        return if (removed != null) listOf(removed) else emptyList()
    }

    /**
     * Removes the [item].
     *
     * @return removed item
     * */
    fun takeOff(item: Item?): List<Item> {
        if (item == null) return emptyList()
        val part = equip.toList().find { it.second == item }?.first
        return if (part != null) takeOff(part) else emptyList()
    }

    private fun equip(item: Item, part: Int): List<Item> {
        val removed = takeOff(part)
        equip[part] = item
        return removed
    }

    /**
     * Equip the [item]
     *
     * @return removed items
     * */
    fun equip(item: Item) = when (item) {
        is Weapon -> take(item)
        is Helmet -> equip(item, HEAD)
        is BodyArmour -> equip(item, BODY)
        is Pants -> equip(item, LEGS)
        is Shield -> putOnShield(item)
        else            -> throw RuntimeException("Item is not belongs to any known armor or weapon class")
    }

    private fun putOnShield(item: Shield): List<Item> {
        val removed = mutableListOf<Item>()
        val equippedInMainHand = get(MHAND)
        if (equippedInMainHand is Weapon && equippedInMainHand.twoHanded) {
            removed.addAll(takeOff(MHAND))
        }
        removed.addAll(equip(item, SHAND))
        return removed.toList()
    }

    private fun take(item: Weapon): List<Item> {
        val removed = mutableListOf<Item>()
        if (item.twoHanded) {
            removed.addAll(takeOff(SHAND))
        }
        removed.addAll(equip(item, MHAND))
        return removed.toList()
    }

    //tostring :


    companion object {
        /**
         * Field number for [get] indicating helmets
         * */
        val HEAD    = 0

        /**
         * Field number for [get] indicating body armor
         * */
        val BODY    = 1

        /**
         * Field number for [get] indicating pants
         * */
        val LEGS    = 2

        /**
         * Field number for [get] indicating com.cornel.shadowrooms.main hand item
         * */
        val MHAND   = 3

        /**
         * Field number for [get] indicating second hand item
         * */
        val SHAND   = 4
    }
}