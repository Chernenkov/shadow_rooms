package com.cornel.shadowrooms.core.entity.player

import com.cornel.shadowrooms.core.entity.Entity
import com.cornel.shadowrooms.core.item.armour.helmet.MythrilHelmet
import com.cornel.shadowrooms.core.item.weapon.Weapon
import com.cornel.shadowrooms.core.item.potion.Effect
import com.cornel.shadowrooms.core.item.potion.EffectType
import com.cornel.shadowrooms.core.item.weapon.sword.BronzeSword
import com.cornel.shadowrooms.core.item.weapon.sword.BronzeTwoHandedSword

open class Player(
        name: String,
        level: Int = 1,
        baseHp: Int = 100,
        hp: Int = baseHp,
        var exp: Int = 0,
        var stats: Stats = Stats(),
        var inventory: PlayerInventory = PlayerInventory(mutableListOf()),
        var equip: Equip = Equip(mutableMapOf(Equip.MHAND to BronzeSword()))
) : Entity(name, level, hp, baseHp) {

    var statsEffect = arrayOf(0, 0, 0, 0, 0, 0)

    override fun injury(hp: Int) { this.hp -= (hp * (1 - equip.getProtection())).toInt() }

    override fun heal(hp: Int) { this.hp = Math.min(getMaxHp(), this.hp + hp) }

    override fun getMaxHp(): Int {
        val endMultiplier = 1 + stats[Stats.END] * END_HP_MOD
        return Math.floor(baseHp * endMultiplier).toInt()
    }

    /**
     * Increase player experience points by [exp].
     * */
    protected fun addExp(exp: Int) { this.exp += exp }

    /**
     * Get experience amount needed to level up.
     * */
    fun getLevelUpExpAmount(): Int {
        var exp = 100
        for (i in 1..level) {
            exp *= i
        }
        return exp
    }

    /**
     * Increase player level, add 1 to [upStat] and fully heal.
     * */
    protected fun levelUp(upStat: Int) {
        level += 1
        stats.add(upStat)
        heal()
    }

    override fun dice(): Double {
        val frtMultiplier = 1 + stats.frt * FRT_DICE_MOD
        return Math.random() * frtMultiplier
    }

    override fun dodge(): Double {
        val dexMultiplier   = stats.dex * DEX_DODGE_MOD
        val personDodge     = BASE_DODGE_CHANCE + dice() * dexMultiplier
        return Math.min(personDodge, MAX_DODGE_CHANCE)
    }

    override fun hit(what: Entity): Boolean {
        // If a weapon isn't equipped, player uses his hands
        val equipped        = equip.mainHand
        val weaponChance    = (equipped as Weapon?)?.hitChance ?: FIST_HIT_CHANCE
        val dexMultiplier   = 1 + stats.dex * DEX_HIT_MOD
        val personChance    = dice() * dexMultiplier
        val resultChance    = Math.min(personChance + weaponChance, MAX_HIT_CHANCE)

        val entityDodgeChance = what.dodge()

        val hit = resultChance - entityDodgeChance

        return if (hit > 0) {
            //TODO Armor damage decreasing
            val strMultiplier = 1 + (stats[Stats.STR] * STR_HIT_MOD)
            val hitDamage = Math.floor(equip.getDamage() * strMultiplier).toInt()
            what.injury(hitDamage)
            true
        } else {
            false
        }
    }

    override fun beforeTurn() {
        heal(HP_REGEN_TURN)
//        applyEffects()
    }

    override fun afterTurn() {

    }

    /**
     * Lifecycle method to make actions after each room
     * */
    fun afterRoom() {
        if (exp > getLevelUpExpAmount()) {
            //TODO Levelup
        }
        heal(HP_REGEN_ROOM)
    }


    fun applyEffects(effects: Array<Effect>) {
        statsEffect.fill(0)
        effects.forEach {
            when(it.type) {
                EffectType.STRENGTH     -> statsEffect[Stats.STR] += it.strength
                EffectType.INTELLIGENCE -> statsEffect[Stats.INT] += it.strength
                EffectType.DEXTRITY     -> statsEffect[Stats.DEX] += it.strength
                EffectType.POISON       -> injury(it.strength)
                EffectType.RAGE         -> statsEffect[Stats.STR] += it.strength
                EffectType.FORTUNE      -> statsEffect[Stats.FRT] += it.strength
                EffectType.ENDURANCE    -> statsEffect[Stats.END] += it.strength
                EffectType.REGENERATION -> heal(it.strength)
            }
            it.duration--
        }
    }

    fun getStatusString(): String = "Ты ${getHpString()}.\n\n" +
            "Характеристики:\n\n" +
                "Сила: ${stats.str}\n" +
                "Ловкость: ${stats.dex}\n" +
                "Выносливость: ${stats.end}\n" +
                "Удача: ${stats.frt}\n"

    companion object {
        private val STR_HIT_MOD     = 0.15
        private val DEX_HIT_MOD     = 0.05
        private val DEX_DODGE_MOD   = 0.1
        private val END_HP_MOD      = 0.2
        private val INT_DMG_MOD     = 0.2
        private val FRT_DICE_MOD    = 0.05

        private val MAX_HIT_CHANCE      = 0.95
        private val FIST_HIT_CHANCE     = 0.6
        private val BASE_DODGE_CHANCE   = 0.05
        private val MAX_DODGE_CHANCE    = 0.8
        private val HP_REGEN_TURN       = 1
        private val HP_REGEN_ROOM       = 10
    }
}