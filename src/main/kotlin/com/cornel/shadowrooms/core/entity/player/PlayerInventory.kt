package com.cornel.shadowrooms.core.entity.player

import com.cornel.shadowrooms.core.common.Inventory
import com.cornel.shadowrooms.core.item.Item
import com.cornel.shadowrooms.core.item.weapon.sword.MythrilSword
import com.cornel.shadowrooms.core.item.weapon.sword.SteelSword

/**
 * @property space free space available in inventory
 * */
class PlayerInventory(items: MutableList<Item> = mutableListOf()) : Inventory(items) {
    val maxSize = 25
    val space = maxSize - size

    override fun add(item: Item): Boolean = if (space >= item.invSize) items.add(item) else false

    override fun remove(item: Item)= items.remove(item)

    override fun replace(item: Item, with: Item): Boolean {
        val replacedItem = items.find { it == with }
        if (replacedItem != null) {
            if (item.invSize <= space - replacedItem.invSize && items.remove(replacedItem)) {
                return items.add(item)
            }
        }
        return false
    }
    
    fun getSpaceString() = when {
        space == maxSize -> "пустой"
        space == 0 -> "полон"
        space < maxSize * 0.25 -> "почти полный"
        space < maxSize * 0.75 -> "занят наполовину"
        space < maxSize -> "почти пустой"
        else -> "почти пустой"
    }
}