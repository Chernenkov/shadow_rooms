package com.cornel.shadowrooms.core.entity.player

/**
 * Defines player characteristics and methods to work with it.
 *
 * @property stats an immutable array of stored stats
 * */
class Stats(strength: Int       = 0,
            dexterity: Int      = 0,
            endurance: Int      = 0,
            intelligence: Int   = 0,
            fortune: Int        = 0) {

    val stats = arrayOf(strength, dexterity, endurance, intelligence, fortune)

    val str: Int
        get() = stats[STR]
    var dex = dexterity
        get() = stats[DEX]
    var end = endurance
        get() = stats[END]
    var int = intelligence
        get() = stats[INT]
    var frt = fortune
        get() = stats[FRT]

    /**
     * Get stored stat value with [stat] field number
     *
     * You can use square brackets (Indexed access operator) to get any stats
     *
     * @param stat A stat defined in the companion object
     * @return An integer value equals to requested stat
     * */
    operator fun get(stat: Int): Int = stats[stat]

    /**
     * Increases the value of selected [stat]
     *
     * */
    fun add(stat: Int) = stats[stat]++

    companion object {
        /**
         * Field number for [get] and [add] indicating character strength
         * */
        val STR = 0
        /**
         * Field number for [get] and [add] indicating character dexterity
         * */
        val DEX = 1
        /**
         * Field number for [get] and [add] indicating character endurance
         */
        val END = 2
        /**
         * Field number for [get] and [add] indicating character intelligence
         */
        val INT = 3
        /**
         * Field number for [get] and [add] indicating character luck (fortune)
         * */
        val FRT = 4
    }
}

