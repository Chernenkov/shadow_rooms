package com.cornel.shadowrooms.core.item

/**
 * The base prototype of any item
 *
 * @param name      Name of the item
 * @param amount    Amount of the current item
 * @param cost      Value of the item in coins
 * @param invSize   Space occupied by the item in the owner's backpack
 * @param stackSize Max amount of items in a single cell
 * @param sellable  Determines whether it's possible to sell the item
 * */
abstract class Item(val name: String,
                    val amount: Int         = 1,
                    val cost: Int           = 0,
                    val invSize: Int        = 1,
                    val stackSize: Int      = 1,
                    val sellable: Boolean   = true) {
    override fun toString(): String {
        val sb = StringBuilder()
        sb.append(name)
        if (stackSize > 1) {
            sb.append(", $amount шт.")
        }
        return sb.toString()
    }
}
