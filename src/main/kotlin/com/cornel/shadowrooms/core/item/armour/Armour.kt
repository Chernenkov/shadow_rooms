package com.cornel.shadowrooms.core.item.armour

import com.cornel.shadowrooms.core.item.Item
/**
 *
 * Armour
 *
 * The base prototype of any armour item
 * Field 'defensePoints' added, determines the number of defense points,
 * it's Double type, because many potions, scrolls have effect on defense of armour
 * and this effect is realized by multiplying and dividing this value by some other
 * floating point numbers.
 *
 * @param name          Name of the armour
 * @param cost          Value of the item in coins
 * @param invSize       Cells occupied by the item in backpack
 * @param defensePoints
 *
 */

abstract class Armour(name:String,
                      cost: Int,
                      invSize: Int,
                      val defensePoints: Double
) : Item(name, 1, cost, invSize, 1, true)