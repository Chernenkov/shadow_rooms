package com.cornel.shadowrooms.core.item.armour.bodyarmour

import com.cornel.shadowrooms.core.item.armour.Armour

/**
 *
 * Bodyarmour
 *
 * The base prototype of any type of body armour
 * invSize for this type of armour is set to 4
 * stackSize for this type of armour is set to 15
 *
 * @param name          Name of current item
 * @param amount        Amount of current item
 * @param cost          Cost of current item in coins
 * @param defensePoints Determines number of defense points
 *
 */
abstract class BodyArmour(
        name: String,
        cost: Int,
        defensePoints: Double
): Armour(name,  cost, 5, defensePoints)