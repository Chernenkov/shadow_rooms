package com.cornel.shadowrooms.core.item.armour.bodyarmour

import com.cornel.shadowrooms.core.item.armour.Armour

// invSize is 3 by default
/**
 * LeatherBodyArmour
 *
 * Class describes one item of leather body armour, derives from bodyarmour
 * By default invSize is determined to be 3 cells in backpack.
 */
class LeatherBodyArmour() : BodyArmour("Кожаный Нагрудник", 350, 0.07)