package com.cornel.shadowrooms.core.item.armour.bodyarmour

import com.cornel.shadowrooms.core.item.armour.Armour

class MythrilBodyArmour() : BodyArmour("Мифриловый Нагрудник", 2800, 0.28)