package com.cornel.shadowrooms.core.item.armour.bodyarmour

import com.cornel.shadowrooms.core.item.armour.Armour

/**
 * SteelBodyArmour
 *
 * Class describes one item of steel body armour, derives from bodyarmour
 * invSize is determined to be 5 cells in backpack
 *
 */
class SteelBodyArmour() : BodyArmour("Стальная Броня", 1400, 0.21)