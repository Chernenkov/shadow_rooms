package com.cornel.shadowrooms.core.item.armour.bodyarmour

import com.cornel.shadowrooms.core.item.armour.Armour

class WroughtIronBodyArmor() : BodyArmour("Кованый Железный Нагрудник", 700, 0.14)