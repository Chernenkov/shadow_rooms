package com.cornel.shadowrooms.core.item.armour.helmet

import com.cornel.shadowrooms.core.item.armour.Armour

/**
 *
 * Helmet
 *
 * The base prototype for helmets
 * invSize for this type of armour is determined to be 2 points
 * stackSize for this type of armour is determined to be 10 points
 *
 * @param name          Name of item
 * @param cost          Value of the item in coins
 * @param defensePoints Determines defense points for helmet item
 */
abstract class Helmet(
        name: String,
        cost: Int,
        defensePoints: Double
) : Armour(name, cost, 2, defensePoints)
