package com.cornel.shadowrooms.core.item.armour.pants

import com.cornel.shadowrooms.core.item.armour.Armour

/**
 *
 * pants
 *
 * The base prototype for all legs armour
 * invSize is set to 3
 * stackSize is set to 10
 *
 * @param name          Name of item
 * @param cost          Cost of current item in coins
 * @param defensePoints Determines number of defense points
 *
 */
abstract class Pants(
        name: String,
        cost: Int,
        defensePoints: Double
) : Armour(name, cost, 4, defensePoints)