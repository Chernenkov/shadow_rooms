package com.cornel.shadowrooms.core.item.armour.shield

import com.cornel.shadowrooms.core.item.armour.Armour

/**
 *
 * Shield
 *
 * The base prototype of any shield item
 * invSize is set to 3
 * stackSize is set to 20
 *
 * @param name          Name of item
 * @param cost          Cost of current item in coins
 * @param defensePoints Determine number of defense points
 */
abstract class Shield(
        name: String,
        cost: Int,
        defensePoints: Double
) : Armour(name, cost, 3, defensePoints)