package com.cornel.shadowrooms.core.item.potion

class Effect(val type: EffectType,
             var duration: Int,
             val strength: Int)