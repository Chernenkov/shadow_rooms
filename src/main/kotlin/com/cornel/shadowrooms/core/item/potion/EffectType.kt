package com.cornel.shadowrooms.core.item.potion

enum class EffectType {
    STRENGTH,
    INTELLIGENCE,
    ENDURANCE,
    DEXTRITY,
    POISON,
    RAGE,
    REGENERATION,
    FORTUNE}