package com.cornel.shadowrooms.core.item.potion

import com.cornel.shadowrooms.core.item.Item

/**
 * Prototype of any potions
 *
 * @param name              Name of the Potion
 * @param amount            Amount of the current potion
 * @param cost              Value of the potion in coins
 * @param invSize           Space occupied by the potion in the backpack
 * @param stackSize         Max amount in a single cell
 * @param sellable          Possibility of sale
 * @param timeOfInfluence   Time of potion effect
 * @param effects           Array that contains all effects values
 * */

abstract class Potion(name: String,
                      amount: Int,
                      cost: Int = 4,
                      val timeOfInfluence: Int,
                      sellable: Boolean = true): Item(name,amount,cost,1,4)