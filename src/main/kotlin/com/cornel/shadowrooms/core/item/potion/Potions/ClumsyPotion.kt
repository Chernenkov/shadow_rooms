package com.cornel.shadowrooms.core.item.potion.Potions

import com.cornel.shadowrooms.core.item.potion.Effect
import com.cornel.shadowrooms.core.item.potion.EffectType
import com.cornel.shadowrooms.core.item.potion.Potion

/**
 * Prototype of any potions
 *
 * @param cost              Cost of potion
 * @param valueOfEffect     Set value of potion
 * @param sellable          Possibility of sale
 * @param timeOfInfluence   Set duration of an effect
 * @param strengthAccuracy  Set strength of Accuracy
 * @param strengthDextrity  Set strenght of Dextrity
 * */

class ClumsyPotion (amount: Int,
                    strength: Int = 4,
                    cost: Int = 5,
                    timeOfInfluence: Int = 3): Potion("Magic potion of Clumsy",amount,cost,timeOfInfluence,true)  {
    val arrayOfEffects = arrayOf(Effect(EffectType.DEXTRITY, timeOfInfluence, strength))
}