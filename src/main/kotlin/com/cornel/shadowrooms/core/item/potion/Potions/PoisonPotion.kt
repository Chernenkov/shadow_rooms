package com.cornel.shadowrooms.core.item.potion.Potions

import com.cornel.shadowrooms.core.item.potion.Effect
import com.cornel.shadowrooms.core.item.potion.EffectType
import com.cornel.shadowrooms.core.item.potion.Potion

/**
 * Prototype of any potions
 *
 * @param cost              Cost of potion
 * @param valueOfEffect     Set value of potion
 * @param sellable          Possibility of sale
 * @param timeOfInfluence   Set duration of an effect
 * */

class PoisonPotion(amount: Int,
                   strength: Int = 3,
                   cost: Int = 1,
                   timeOfInfluence: Int = 3): Potion("Magic potion of Poison",amount,cost,timeOfInfluence,true) {
    val arrayOfEffects = arrayOf(Effect(EffectType.POISON, timeOfInfluence, strength))
}