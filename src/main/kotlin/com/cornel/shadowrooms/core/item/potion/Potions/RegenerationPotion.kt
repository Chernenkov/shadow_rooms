package com.cornel.shadowrooms.core.item.potion.Potions

import com.cornel.shadowrooms.core.item.potion.Effect
import com.cornel.shadowrooms.core.item.potion.EffectType
import com.cornel.shadowrooms.core.item.potion.Potion

/**
 * Prototype of any potions
 *
 * @param cost              Cost of potion
 * @param valueOfEffect     Set value of potion
 * @param sellable          Possibility of sale
 * @param timeOfInfluence   Set duration of an effect
 * */

class RegenerationPotion (amount: Int,
                          strength: Int = 5,
                          cost: Int = 4,
                          timeOfInfluence: Int = 3): Potion("Magic potion of Regeneration",amount,cost,timeOfInfluence,true) {
    val arrayOfEffects = arrayOf(Effect(EffectType.REGENERATION, timeOfInfluence, strength))
}