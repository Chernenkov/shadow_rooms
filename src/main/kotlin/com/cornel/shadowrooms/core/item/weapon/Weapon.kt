package com.cornel.shadowrooms.core.item.weapon

import com.cornel.shadowrooms.core.item.Item

/**
 * The prototype of any weapon
 *
 * @property damage Base damage
 * @property hitChance Hit chance modifier. The value sums with person hit chance,
 *                     so it must be negative if weapon is big and massive or positive otherwise
 * @property twoHanded Does the weapon take two hands
 * */

abstract class Weapon(name: String,
                      val damage: Int,
                      val hitChance: Double,
                      cost: Int,
                      invSize: Int,
                      sellable: Boolean,
                      val twoHanded: Boolean = false): Item(name, 1, cost, invSize, 1, sellable)