package com.cornel.shadowrooms.core.item.weapon.knife

import com.cornel.shadowrooms.core.item.weapon.Weapon

class BronzeKnife : Weapon("Бронзовый Клинок", 15, 0.2, 110, 1, true, false)