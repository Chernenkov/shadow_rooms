package com.cornel.shadowrooms.core.item.weapon.knife

import com.cornel.shadowrooms.core.item.weapon.Weapon

class SteelKnife : Weapon("Стальной Клинок", 45, 0.2, 440, 8, true, false)