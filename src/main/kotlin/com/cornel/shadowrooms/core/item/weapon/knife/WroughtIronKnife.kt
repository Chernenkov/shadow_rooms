package com.cornel.shadowrooms.core.item.weapon.knife

import com.cornel.shadowrooms.core.item.weapon.Weapon

class WroughtIronKnife : Weapon("Кованый Клинок", 30, 0.2, 220, 5, true, false)