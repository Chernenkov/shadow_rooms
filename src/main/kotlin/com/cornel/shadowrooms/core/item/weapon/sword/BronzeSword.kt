package com.cornel.shadowrooms.core.item.weapon.sword

import com.cornel.shadowrooms.core.item.weapon.Weapon

class BronzeSword : Weapon("Бронзовый Меч", 20, 0.0, 150, 2, true, false)