package com.cornel.shadowrooms.core.item.weapon.sword

import com.cornel.shadowrooms.core.item.weapon.Weapon

class BronzeTwoHandedSword : Weapon("Двуручный Бронзовый Меч", 30, -0.15, 270, 4, true, true)