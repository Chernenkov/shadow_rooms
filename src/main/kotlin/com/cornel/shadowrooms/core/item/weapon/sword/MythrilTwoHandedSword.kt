package com.cornel.shadowrooms.core.item.weapon.sword

import com.cornel.shadowrooms.core.item.weapon.Weapon

class MythrilTwoHandedSword : Weapon("Двуручный Мифриловый Меч", 120, -0.05, 2160, 12, true, true)