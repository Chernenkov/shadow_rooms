package com.cornel.shadowrooms.core.item.weapon.sword

import com.cornel.shadowrooms.core.item.weapon.Weapon

class SteelTwoHandedSword : Weapon("Стальной Двуручный Меч", 90, -0.15, 1080, 9, true, true)