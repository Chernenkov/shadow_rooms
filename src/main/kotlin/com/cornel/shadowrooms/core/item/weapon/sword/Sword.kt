package com.cornel.shadowrooms.core.item.weapon.sword

import com.cornel.shadowrooms.core.item.weapon.Weapon

abstract class Sword(name: String,
                   damage: Int,
                   hitChance: Double,
                   cost: Int,
                   invSize: Int,
                   sellable: Boolean,
                   twoHanded: Boolean)
    : Weapon(name, damage, hitChance, cost, invSize, sellable, twoHanded)