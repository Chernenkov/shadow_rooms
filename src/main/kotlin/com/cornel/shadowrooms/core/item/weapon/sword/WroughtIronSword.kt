package com.cornel.shadowrooms.core.item.weapon.sword

import com.cornel.shadowrooms.core.item.weapon.Weapon

class WroughtIronSword : Weapon("Кованый Меч", 40, 0.0, 300, 4, true, false)