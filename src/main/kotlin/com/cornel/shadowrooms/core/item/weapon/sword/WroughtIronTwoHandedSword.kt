package com.cornel.shadowrooms.core.item.weapon.sword

import com.cornel.shadowrooms.core.item.weapon.Weapon

class WroughtIronTwoHandedSword : Weapon("Кованый Двуручный Меч", 60, -0.15, 540, 6, true, true)