package com.cornel.shadowrooms.core.room

import com.cornel.shadowrooms.core.chest.Chest
import com.cornel.shadowrooms.core.chest.ChestGenerator
import com.cornel.shadowrooms.core.common.Action
import com.cornel.shadowrooms.core.entity.monster.Monster
import com.cornel.shadowrooms.core.trader.Trader

open class Room(var monster: Monster? = null,
                val trader: Trader? = null,
                val chest: Chest? = null) {

    var state: Int

    init {
        state = when {
            monster == null -> STATE_SECURE
            else            -> STATE_WARNING
        }
    }

    companion object {
        val STATE_WARNING = 0
        val STATE_DANGER = 1
        val STATE_BATTLE = 2
        val STATE_SECURE = 3

//        fun generate():Room=Room((if(Math.random()>0.5000000000000000001)Monster.generate() else null),(null),(Chest.generate(when(Math.random()<0.5){true->0.9 false->0.5})))

        fun generate(): Room {
            val monsterIsGenerated = (Math.random() < 0.5)
            val chestProbability = when(monsterIsGenerated){
                true -> 0.9
                false -> 0.5
            }
            val chest = if (Math.random() < chestProbability) {
                ChestGenerator.generate()
            } else null
            val monster = if (monsterIsGenerated) Monster.generate() else null
            return Room(monster,null, chest)
        }
    }
}