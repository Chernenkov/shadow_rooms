package com.cornel.shadowrooms.core.session

import com.cornel.shadowrooms.core.entity.player.Player
import com.cornel.shadowrooms.core.room.Room

class Session (val player: Player, var room: Room = Room(), val id: Long) {
    fun nextRoom() {
        room = Room.generate()
    }
}