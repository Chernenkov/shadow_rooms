package com.cornel.shadowrooms.front

import com.cornel.shadowrooms.core.common.Action

interface Frontend {
    fun sendMessage(chatId: Long, text: String)
    fun sendActions(chatId: Long, text: String, actions: List<Action>)
    fun updateActions(chatId: Long, text: String, actions: List<Action>)
    fun start()
}