package com.cornel.shadowrooms.front.telegram

import com.cornel.shadowrooms.Application
import com.cornel.shadowrooms.core.common.Action
import com.cornel.shadowrooms.core.common.ActionType
import com.cornel.shadowrooms.front.Frontend
import com.google.gson.Gson
import org.telegram.telegrambots.TelegramBotsApi
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText
import org.telegram.telegrambots.api.objects.Message
import org.telegram.telegrambots.api.objects.Update
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton
import org.telegram.telegrambots.bots.TelegramLongPollingBot

class TelegramBot(private val context: Application) : TelegramLongPollingBot(), Frontend {

    private val gson = Gson()

    private val lastActionMessages = hashMapOf<Long, Message>()

    override fun getBotToken() = "496479958:AAHwwIBh21hTjDj0JG6rAQaxVR-26HsqbyE"

    override fun getBotUsername() = "Shadow Rooms"

    override fun onUpdateReceived(update: Update) {
        if (update.hasCallbackQuery()) {
            val chatId = update.callbackQuery.from.id.toLong()
            val data = update.callbackQuery.data.split(':')
            val actionType = ActionType.valueOf(data[0])
            val payload = if (data.size > 1) data[1] else "empty"
            context.doAction(chatId, Action("", actionType, payload))
        } else if (update.hasMessage()) {
            println("Message from ${update.message.from.userName}, text: ${update.message.text}")
            when (update.message.text) {
                "/start" -> onStartCommand(update)
            }
        }
    }

    override fun sendMessage(chatId: Long, text: String) {
        val sendMessage = SendMessage()
        sendMessage.chatId = chatId.toString()
        sendMessage.text = text
        execute(sendMessage)
    }

    override fun sendActions(chatId: Long, text: String, actions: List<Action>) {
        //Removing action buttons from previous message
        lastActionMessages[chatId]?.apply {
            updateActions(chatId, this.text, emptyList())
        }

        //Sending new action message
        val message = SendMessage()
        val keyboardMarkup = InlineKeyboardMarkup()
        keyboardMarkup.keyboard = createMarkup(actions)
        message.chatId = chatId.toString()
        message.text = text
        message.replyMarkup = keyboardMarkup
        lastActionMessages[chatId] = execute(message)
    }

    override fun updateActions(chatId: Long, text: String, actions: List<Action>) {
        val message = lastActionMessages[chatId]!!
        val editMessage = EditMessageText()
        editMessage.chatId = message.chatId.toString()
        editMessage.messageId = message.messageId
        editMessage.text = text
        editMessage.replyMarkup = InlineKeyboardMarkup().setKeyboard(createMarkup(actions))
        execute(editMessage)
    }

    private fun createMarkup(actions: List<Action>): List<List<InlineKeyboardButton>> {
        val buttons = mutableListOf<List<InlineKeyboardButton>>()

        for (i in 0 until actions.size step 2) {
            val line = mutableListOf(
                    InlineKeyboardButton()
                            .setCallbackData("${actions[i].type}:${actions[i].payload}")
                            .setText(actions[i].name)
            )
            if (i + 1 < actions.size) {
                // Not the last type, we can put second button in line
                line.add(InlineKeyboardButton()
                        .setCallbackData("${actions[i + 1].type}:${actions[i + 1].payload}")
                        .setText(actions[i + 1].name)
                )
            }
            buttons.add(line as List<InlineKeyboardButton>)
        }

        println(buttons)

        return buttons
    }

    private fun onStartCommand(update: Update) {
        val user = update.message.from
        sendMessage(update.message.chatId, "Добро пожаловать, ${user.firstName}!\n\n" +
                "Ты забрёл в это место в поисках сокровищ, приключений и славы. " +
                "Помни, что Тёмные Комнаты вознаградят только самых смелых, сильных и удачливых.\n\n" +
                "Ты заходишь в первую комнату...")
        context.registerUser("${user.firstName} ${user.lastName}", update.message.chatId)
    }

    override fun start() {
        TelegramBotsApi().registerBot(this)
        println("TelegramBot is running...")
    }
}
