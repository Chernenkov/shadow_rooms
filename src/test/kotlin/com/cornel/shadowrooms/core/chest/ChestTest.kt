package com.cornel.shadowrooms.core.chest

import com.cornel.shadowrooms.core.item.Item
import com.cornel.shadowrooms.core.item.armour.bodyarmour.SteelBodyArmour
import com.cornel.shadowrooms.core.item.armour.helmet.MythrilHelmet
import com.cornel.shadowrooms.core.item.armour.helmet.SteelHelmet
import com.cornel.shadowrooms.core.item.armour.pants.SteelPants
import com.cornel.shadowrooms.core.item.weapon.knife.SteelKnife
import org.testng.Assert.*
import org.testng.annotations.Test


class ChestTest(){
    val items = arrayListOf(SteelHelmet(), SteelBodyArmour(), MythrilHelmet(), SteelKnife())
    val chest = Chest(items)
    @Test
    fun testChestNull(){
        assertNotNull(chest.fillChest())
    }


    @Test
    fun testChestReplace(){
        val chestForReplace = Chest(items)
        chestForReplace.replace(SteelHelmet(),SteelPants())
        assertNull(chestForReplace.items.find { it == SteelHelmet() })
    }
}