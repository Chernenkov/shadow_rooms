//package core.common.player
//
//import com.cornel.shadowrooms.Stats
//import org.testng.Assert.*
//import org.testng.annotations.Test
//
//class StatsTest {
//    var stats = Stats(1, 2, 3, 4, 5)
//
//    @Test
//    fun testGet() {
//        assertEquals(stats[Stats.STR], 1)
//        assertEquals(stats[Stats.DEX], 2)
//        assertEquals(stats[Stats.END], 3)
//        assertEquals(stats[Stats.INT], 4)
//        assertEquals(stats[Stats.FRT], 5)
//    }
//
//    @Test(expectedExceptions = arrayOf(ArrayIndexOutOfBoundsException::class))
//    fun testGetOutOfBounds() {
//        stats[6]
//    }
//
//    @Test
//    fun testAdd() {
//        val stats2 = stats.add(Stats.END)
//        assertEquals(stats2[Stats.STR], 1)
//        assertEquals(stats2[Stats.DEX], 2)
//        assertEquals(stats2[Stats.END], 4)
//        assertEquals(stats2[Stats.INT], 4)
//        assertEquals(stats2[Stats.FRT], 5)
//
//        val stats3 = stats.add(Stats.FRT)
//        assertEquals(stats3[Stats.STR], 1)
//        assertEquals(stats3[Stats.DEX], 2)
//        assertEquals(stats3[Stats.END], 3)
//        assertEquals(stats3[Stats.INT], 4)
//        assertEquals(stats3[Stats.FRT], 6)
//    }
//}