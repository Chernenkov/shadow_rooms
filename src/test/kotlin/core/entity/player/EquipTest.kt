

import com.cornel.shadowrooms.core.entity.player.Equip
import com.cornel.shadowrooms.core.item.armour.bodyarmour.BodyArmour
import com.cornel.shadowrooms.core.item.armour.helmet.Helmet
import com.cornel.shadowrooms.core.item.armour.pants.Pants
import com.cornel.shadowrooms.core.item.armour.shield.Shield
import com.cornel.shadowrooms.core.item.weapon.sword.Sword
import org.testng.annotations.Test

import org.testng.Assert.*

class EquipTest {
    private var testHelm: Helmet
    private var testBody: BodyArmour
    private var equip: Equip

    init {
        testHelm = object : Helmet("testHelm", 1, 1.0) {}
        testBody = object : BodyArmour("testBodyArmor", 1, 0.0) {}
        equip = Equip(mutableMapOf(Equip.HEAD to testHelm, Equip.BODY to testBody))
    }

    fun prepare() {
        testHelm = object : Helmet("testHelm", 1, 0.0) {}
        testBody = object : BodyArmour("testBodyArmor", 1, 0.0) {}
        equip = Equip(mutableMapOf(Equip.HEAD to testHelm, Equip.BODY to testBody))
    }

    @Test
    fun testGet() {
        prepare()

        assertEquals(equip.head, testHelm, "The helmet is equipped")
        assertEquals(equip.body, testBody, "The body armor is equipped")
        assertNull(equip.legs, "No pants equipped")
    }

    @Test
    fun testTakeOff() {
        prepare()
        equip.takeOff(equip.head)
        assertNull(equip.head, "Helm is off")
        assertEquals(equip.body, testBody, "The body armor is still there")
    }

    @Test
    fun testPut() {
        prepare()
        val pants = object : Pants("testPants", 1, 0.0) {}
        equip.equip(pants)

        assertEquals(equip.legs, pants, "pants is equipped")
        assertEquals(equip.body, testBody, "The body armor is still there")
    }

    @Test
    fun testTake() {
        prepare()
        val test1HSword = object : Sword("test1HSword", 0, 0.0, 123, 1, false, false) {}
        val test2HSword = object : Sword("test2HSword", 0, 0.0, 123, 1, false, true) {}
        val testShield  = object : Shield("testShield", 1, 0.0) {}

//        Equip sword and shield

        equip.equip(testShield)
        equip.equip(test1HSword)

        assertEquals(equip.mainHand, test1HSword, "Sword is equipped")
        assertEquals(equip.secondHand, testShield, "shield is equipped")

//        Equip 2-h sword, should take off 1-h sword and shield

        equip.equip(test2HSword)

        assertEquals(equip.mainHand, test2HSword, "Sword is equipped")
        assertNull(equip.secondHand, "shield is removed")
    }

    @Test
    fun testGetDamage() {
        prepare()
        val testSword = object : Sword("testSword", 10, 0.0, 123, 1, false, false) {}

        assertEquals(equip.getDamage(), 0, "Damage without weapon is 0")

        equip.equip(testSword)
        assertEquals(equip.getDamage(), testSword.damage, "Damage with weapon is correct")
    }
}