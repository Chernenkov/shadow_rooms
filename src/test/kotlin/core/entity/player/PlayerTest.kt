package core.entity.player

import com.cornel.shadowrooms.core.entity.player.Equip
import com.cornel.shadowrooms.core.entity.player.Player
import com.cornel.shadowrooms.core.entity.player.PlayerInventory
import com.cornel.shadowrooms.core.entity.player.Stats
import org.testng.Assert.*
import org.testng.annotations.Test

class PlayerTest {
    val p = Player("testPlayer", 15, 50, 100, 32768,
            Stats(0, 0, 0, 0, 0),
            PlayerInventory(), Equip())

    @Test
    fun testInjury() {
        val before = p.hp
        val dmg = 30
        p.injury(dmg)
        assertEquals(p.hp, before - dmg, "Damage done")
    }

    @Test
    fun testHeal() {
        val before = p.hp
        val heal = 12
        p.heal(heal)
        assertEquals(p.hp, p.getMaxHp())
    }
}