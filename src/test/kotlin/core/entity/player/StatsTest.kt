package core.entity.player

import com.cornel.shadowrooms.core.entity.player.Stats
import org.testng.Assert.*
import org.testng.annotations.Test

class StatsTest {

    fun getStats(): Array<Int> {
        return arrayOf(1, 4, 3, 2, 0)
    }
    @Test
    fun testStats(){
        assertEquals(getStats(), arrayOf(Stats.DEX,Stats.FRT,Stats.INT,Stats.END,Stats.STR))
    }

}